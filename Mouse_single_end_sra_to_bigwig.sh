#!/usr/bin/env sh
HELP="USAGE: sh Mouse_single_end_sra_to_bigwig.sh -s {Sample}"
if [ ! $1 ]  ; then
        echo $HELP
        exit 2
fi


##################

while getopts s: option
  do
    case "${option}"
      in
      s) Sample=${OPTARG};;
    esac
done

fasterq-dump $Sample -e 48 
mv $Sample.fastq  $Sample.fq 
pigz $Sample.fq
bowtie2 -x /home/cheng/Refgenome/mm10_bowtie2/mm10  -p 48 -U $Sample.fq.gz  2> $Sample_bowtie2.log | samtools view -bS - | samtools view -bq 2  | samtools sort  > $Sample.bam
samtools markdup -r $Sample.bam $Sample.rmdup.bam -@48
samtools index $Sample.rmdup.bam
bamCoverage -of bigwig -b $Sample.rmdup.bam -bs 10  --smoothLength 30 -e 200 -p 48 --normalizeUsing RPKM -ignore chrM chrX chrY -o $Sample.bw
rm $Sample.fq.gz
rm $Sample.bam
#done

