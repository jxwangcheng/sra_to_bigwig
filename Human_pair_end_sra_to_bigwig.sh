#!/usr/bin/env sh

HELP="USAGE: sh Human_pair_end_sra_to_bigwig.sh -s {Sample}"
if [ ! $1 ]  ; then
        echo $HELP
        exit 2
fi


##################

while getopts s: option
  do
    case "${option}"
      in
      s) Sample=${OPTARG};;
    esac
done

fasterq-dump $Sample -e 48
mv $Sample\_1.fastq  $Sample\_1.fq 
mv $Sample\_2.fastq  $Sample\_2.fq
pigz $Sample*.fq
bowtie2 -x /index/hg38/bowtie2/hg38.fa   -p 48 -1 $Sample\_1.fq.gz -2 $Sample\_2.fq.gz 2> $Sample\_bowtie2.log | samtools view -bS - | samtools view -bq 2  | samtools sort  > $Sample.bam
samtools markdup -r $Sample.bam $Sample.rmdup.bam -@48
samtools index $Sample.rmdup.bam
bamCoverage -of bigwig -b $Sample.rmdup.bam -bs 10  --smoothLength 30 -e 200 -p 48 --normalizeUsing RPKM -ignore chrM chrX chrY -o $Sample.bw
rm $Sample\_*fq.gz
rm $Sample.bam
#done